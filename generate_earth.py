#!/usr/bin/env python3

# This small script converts the markdown files and the tour.yaml file
# into <div id=[tag]> HTML and Javascript for matching the position
# stuff.

import markdown
from os import path, makedirs
from glob import glob

# user config
input_md_glob = "arch/markdown/*.md"
input_css_glob = "arch/style/*.css"
output_html = "public/google-earth/index.html"
# windows compat
input_md_glob = path.sep.join(input_md_glob.split("/"))
input_css_glob = path.sep.join(input_css_glob.split("/"))
output_html = path.sep.join(output_html.split("/"))
# set outfile
makedirs(path.dirname(output_html), exist_ok=True)

tour_map = {} # tag -> properties
mdfile_map = {} # mdfile -> tag


def parseConf():
    '''
    Parses the config file of the google earth script, and populates the
    tour_map and mdfile_map file.
    '''
    ## Global maps
    with open("arch/tour.conf", 'r') as tour:
        current_tag = None

        for line in tour:
            if line[0] == "#" or len(line) > 3:
                if line.startswith("["):
                    tag = line.split("[")[1].split("]")[0]
                    if tag in tour_map:
                        print(tag, "already defined!")
                        exit(-1)
                    tour_map[tag] = {}
                    current_tag = tag
                    continue

                if "=" in line:
                    key, vals = line.splitlines()[0].split("=")
                    key = key.strip()
                    vals = vals.strip()

                    if key == "latlong":
                        vals == [float(x) for x in vals.split(',')]
                        tour_map[current_tag]["lat"] = vals[0]
                        tour_map[current_tag]["long"] = vals[1]
                    elif key in ("direction", "tilt"):
                        tour_map[current_tag][key] = float(vals)
                    elif key in ("markdown", "style"):
                        tour_map[current_tag][key] = vals
                    elif key == "transitions":
                        tour_map[current_tag][key] = [x.strip() for x in vals.split(",")]

        # Populate file map
        for tag in tour_map:
            f = tour_map[tag]["markdown"]
            if f in mdfile_map:
                print(f, "is declared twice?")
                exit(-1)
            mdfile_map[f] = tag


def writeHeaders():
    '''
    Concat the stylesheets and make the main HTML body.
    '''
    with open(output_html, 'w') as o:
        o.write('''
<html>
    <head>
        <script src="zoom.js"></script>
        <style>
''')       
        for style in glob(input_css_glob):
            with open(style, 'r') as f:
                o.write(f.read())
                o.write('\n')
        o.write('''
        </style>
    </head>
''')

def writeBody():
    with open(output_html, 'a') as outhtml:
        # Generate basic HTML
        outhtml.write('''
        <body>
    ''')
        for md in glob(input_md_glob):
            with open(md, 'r') as mfile:
                text = mfile.read()
                html = markdown.markdown(text)
                tag = mdfile_map[path.basename(md)]
                if 'style' in tour_map[tag]:
                    style = tour_map[tag]['style']
                else:
                    style = "classic"

                outhtml.write("\n<div id='md_%s' class='%s' >" %
                              (tag, style))
                outhtml.write(html)
                outhtml.write("</div>")
        outhtml.write('''
    </body>
</html>
''')


parseConf()

writeHeaders()

writeBody()