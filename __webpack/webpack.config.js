const RemarkHTML = require("remark-html");
const path = require('path');

module.exports = {
    mode: "development",
    entry: './src/index.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.md$/,
                use: [{
                    loader: "html-loader",
                },{
                    loader: "remark-loader",
                    options: {
                        remarkOptions: {
                            plugins: [RemarkHTML],
                        },
                    },
                }],
            },
            {
                test: /\.css$/,
                include: path.resolve(__dirname, 'src/styles'),
                use: ['style-loader', 'css-loader'],
                generator: {
                    filename: 'assets/css-[hash][ext][query]'
                }
            },
            {
                test: /\.(png|jpg|ico)$/i,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/image-[hash][ext][query]'
                }
            },

        ]
    }
};